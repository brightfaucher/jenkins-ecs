terraform {
  required_providers {
    archive = {
      source = "hashicorp/archive"
    }
    aws = {
      version = "~> 3.10"
      source  = "hashicorp/aws"
    }
  }
  required_version = ">= 0.14"
}
