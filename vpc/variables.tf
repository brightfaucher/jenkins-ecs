variable "name" {
  description = "Name for VPC and associated resources"
  type        = string
}

variable "cidr" {
  description = "CIDR block for VPC"
  type        = string
}

variable "azs" {
  description = "AWS availability zones for VPC to span"
  type        = list(string)
}

variable "private_cidrs" {
  description = "List of CIDR blocks to create private subnets for"
  type        = list(string)
}

variable "public_cidrs" {
  description = "List of CIDR blocks to create public subnets for"
  type        = list(string)
}
