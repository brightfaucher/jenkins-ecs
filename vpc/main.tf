module "awsvpc" {
  source = "terraform-aws-modules/vpc/aws"

  name            = var.name
  cidr            = var.cidr
  azs             = var.azs
  private_subnets = var.private_cidrs
  public_subnets  = var.public_cidrs

  enable_dns_support     = true
  enable_dns_hostnames   = true
  enable_nat_gateway     = true
  single_nat_gateway     = false
  one_nat_gateway_per_az = true
}

# Remove all ingress and egress rules from the VPC's default security group.
resource "aws_default_security_group" "this" {
  vpc_id = module.awsvpc.vpc_id
  tags   = { Name = "${var.name}-default" }
}

resource "aws_security_group" "this" {
  name   = "${var.name}-sg"
  vpc_id = module.awsvpc.vpc_id
  tags   = { Name = "${var.name}-sg" }

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}