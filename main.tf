data "aws_caller_identity" "current" {}

locals {
  name       = "jenkins-ecs"
  region     = "us-east-1"
  account_id = data.aws_caller_identity.current.account_id

  vpc_cidr        = "20.10.0.0/16"
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["20.10.1.0/24", "20.10.2.0/24", "20.10.3.0/24"]
  public_subnets  = ["20.10.11.0/24", "20.10.12.0/24", "20.10.13.0/24"]
}

# VPC to contain all resources
module "vpc" {
  source = "./vpc"

  name          = local.name
  cidr          = local.vpc_cidr
  azs           = local.azs
  private_cidrs = local.private_subnets
  public_cidrs  = local.public_subnets
}

# IAM stuff
module "iam" {
  source = "./iam"

  name       = local.name
  region     = local.region
  account_id = local.account_id
}

# ECS Cluster
module "cluster" {
  source = "./cluster"

  name   = local.name
  vpc_id = module.vpc.vpc_id
}
