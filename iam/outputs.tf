output "task_execution_role_name" {
  description = "Name of IAM role created for task execution"
  value       = aws_iam_role.task_execution.name
}

output "task_execution_role_arn" {
  description = "ARN of IAM role created for task execution"
  value       = aws_iam_role.task_execution.arn
}

output "jenkins_master_task_role_name" {
  description = "Name of IAM role created for task instance"
  value       = aws_iam_role.jenkins_master_task.name
}

output "jenkins_master_task_role_arn" {
  description = "ARN of IAM role created for task instance"
  value       = aws_iam_role.jenkins_master_task.arn
}
